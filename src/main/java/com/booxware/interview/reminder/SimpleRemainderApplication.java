package com.booxware.interview.reminder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleRemainderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleRemainderApplication.class, args);
	}
}
