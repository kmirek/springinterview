package com.booxware.interview.reminder;

import com.booxware.interview.reminder.Domain.Remainder;
import com.booxware.interview.reminder.Service.RemainderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestResource {

    @Autowired
    RemainderService service;

    @RequestMapping("/simpleReminder")
    public Remainder getReminder() {
        return service.createReminder();
    }
}
