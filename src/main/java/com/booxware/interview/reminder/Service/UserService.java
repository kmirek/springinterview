package com.booxware.interview.reminder.Service;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private static final String DEFAULT_CURRENT_USER = "Mike";
    private List<String> users;

    @PostConstruct
    public void initialize() {
        users = setUserList();
    }

    public String getCurrentUser() {
        String currentUser = null;
        for (String user : users) {
            if (user.equals(DEFAULT_CURRENT_USER))
              currentUser = user;
        }
        return currentUser;
    }

    private List<String> setUsers() {
        List<String> users = new ArrayList<>();
        users.add("John");
        users.add("Mike");
        users.add("Tonny");
        return users;
    }

    private List<String> setUserList() {
        return new ArrayList<>();
    }
}
