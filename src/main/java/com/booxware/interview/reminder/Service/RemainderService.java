package com.booxware.interview.reminder.Service;

import com.booxware.interview.reminder.Domain.Alarm;
import com.booxware.interview.reminder.Domain.DateDAO;
import com.booxware.interview.reminder.Domain.Remainder;
import com.booxware.interview.reminder.Domain.Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RemainderService {

    @Autowired
    DateDAO dateDAO;

    @Autowired
    Factory reminderFactory;

    @Autowired
    UserService userService;

    public Remainder createReminder() {
        Remainder remainder = reminderFactory.buildRemainder();
        adjustAlarm(remainder.getAlarm());
        return remainder;
    }

    private void adjustAlarm(Alarm alarm) {
        alarm.setTime(dateDAO.getTime());
        alarm.setUser(userService.getCurrentUser());
    }
}
