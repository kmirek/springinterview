package com.booxware.interview.reminder.Domain;

import org.springframework.stereotype.Component;

@Component
public class Factory {

    public Remainder buildRemainder() {
        Remainder remainder = new Remainder();
        remainder.setTitle("Date reminder");
        remainder.setDescription("We meet @Marienplatz. Don't forget to bring flowers!");

        Alarm alarm = new Alarm();
        alarm.setRingTone("Nokia tune");
        remainder.setAlarm(alarm);

        return remainder;
    }
}
