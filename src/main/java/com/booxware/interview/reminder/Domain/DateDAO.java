package com.booxware.interview.reminder.Domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;

@Component
public class DateDAO {

    @Value("${test.someDate}")
    private String longString;

    @Value("${test.dateDelimeter}")
    private String delimeter;

    public String getTime() {
        try {
            return longString.split(delimeter)[1];
        } catch (Exception e) {
            return stacktraceToString(e);
        }
    }

    private String stacktraceToString(Exception e) {
        String result;StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        result = sw.toString();
        return result;
    }

}
