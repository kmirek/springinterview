package com.booxware.interview.reminder.Domain;

public class Remainder {

    String title;
    String description;
    Alarm alarm;

    public Remainder(String title, String description, Alarm alarm) {
        this.title = title;
        this.description = description;
        this.alarm = alarm;
    }

    public Remainder() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Remainder remainder = (Remainder) o;

        if (title != null ? !title.equals(remainder.title) : remainder.title != null) return false;
        if (description != null ? !description.equals(remainder.description) : remainder.description != null) return false;
        return alarm != null ? alarm.equals(remainder.alarm) : remainder.alarm == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (alarm != null ? alarm.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Remainder{" +
                "ringTone='" + title + '\'' +
                ", time='" + description + '\'' +
                ", alarm=" + alarm +
                '}';
    }
}
