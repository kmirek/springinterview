package com.booxware.interview.reminder.Domain;

public class Alarm {

    String ringTone;
    String time;
    private String user;

    public String getRingTone() {
        return ringTone;
    }

    public void setRingTone(String ringTone) {
        this.ringTone = ringTone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "ringTone='" + ringTone + '\'' +
                ", time='" + time + '\'' +
                ", user='" + user + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Alarm alarm = (Alarm) o;

        if (ringTone != null ? !ringTone.equals(alarm.ringTone) : alarm.ringTone != null) return false;
        return time != null ? time.equals(alarm.time) : alarm.time == null;

    }

    @Override
    public int hashCode() {
        int result = ringTone != null ? ringTone.hashCode() : 0;
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
